package com.example.demo.configurations;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class TestAOP {

    @Before("execution(* com.example..*(..))")
    public void test(JoinPoint _joinPoint) {
        System.out.println("ddd: " + _joinPoint);
    }

//    @Pointcut("within(@org.springframework.stereotype.Controller *)")
//    public void controllerBean() {}
//
//    @Pointcut("execution(* *(..))")
//    public void methodPointcut() {}
//
//    @AfterReturning("controllerBean() && methodPointcut() ")
//    public void afterMethodInControllerClass() {
//        System.out.println("after advice..");
//    }

}
